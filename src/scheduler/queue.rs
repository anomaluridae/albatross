/* TODO

    - 4. scheduler
            - LU: Rust channel.
                - combines insert & advance
            - i. queue insertion:
                    - if attached:
                        - check ok in gateKeeper
                        - add to gateKeeper
                            { uuid: { rule, latestInitTime }  }
                    - queue.add (if passed gatekeeper)
            - ii. queue advance:
                    --> next task:
                            - check gatekeeper
                                - should cancel? (e.g. is no longer the latest)
                    --> grab thread from pool
                            - how perform .next() using this thread?
                            - thread ends --> msg back
                    --> msg back received.
                            - to insert into scheduler again.
*/
