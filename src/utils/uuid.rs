use std::{fmt, u64};

#[derive(Copy, Clone, Debug, Eq, Hash, PartialEq)]
pub struct Uuid(pub u64);

impl Uuid {
    pub fn new() -> Self {
        // FIXME: which crate to use? how do Rust uuid?
        Self(7235043567)
    }
}

impl From<Uuid> for u64 {
    fn from(uid: Uuid) -> u64 { uid.0 }
}

impl From<u64> for Uuid {
    fn from(uid: u64) -> Self { Self(uid) }
}

impl fmt::Display for Uuid {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result { write!(f, "{}", self.0) }
}
