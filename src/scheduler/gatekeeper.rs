/*
    TODO:
    - gatekeeper will track the unique ID:
        - uuid of each unique task root.
        - for which task roots are NOT cancelled.
    - task roots are identified by the uuid.
        - a spawned new task root, is not in the dispatched handler,
            but is still a task root in gatekeeper (for it's own subtasks)
    - gatekeeper_uuid is then passed in the context to all children tasks

    - per task being pulled up in the scheduler
        1. check gatekeeper for uuid
        2. if should cancel --> trigger the task.cancel()
        3. if ok --> continue to schedule

    - insertion into the gatekeeper occurs from:
        - bottom-up.
            - e.g. API failure at some child task (in task tree)
        - top-down.
            - based on the dispatch rule.
                e,g. takeLatest
            - gets removed into gatekeeper.
            - then, the cancellation still occurs from bottom-up. so the same procedure applies.
*/