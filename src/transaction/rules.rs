#[derive(Clone, Copy, Debug)]
pub enum TransactionRule {
   PerRule,
   PerAction,
   Unrestricted,
   Custom,
}
