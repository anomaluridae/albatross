
</br>
</br>

# lib name TBD

This library handles client-side state management for Rust-wasm. It's premise is based on allowing the developer to define ordering and concurrency declarations for state updates.

Developer declarations are made at three levels:
1. **Dispatch rules** = How each new dispatch is handled.
2. **Side Effect rules** = How downstream subtasks, from a single dispatch, are coordinated.
2. **Transaction rules** = How state updates downstream of a single dispatch, are handled.

Needs #1 and #2 borrow heavily from the design of Redux-saga, which has a proven model for controlling the flow of side effects per each new dispatch as well as across re-dispatching of the same action.

Need #3 borrows from optimistic concurrency control.

</br>

**IMPORTANT:** This project is currently under development, and is not yet ready for use. The tentative API is defined below for reference only. Roadmap for initial functionality is [found below](./README.md#to-launch-mvp).

</br>
</br>

## Dispatch flow

</br>


1. State updates begin with a dispatched action. This action is typed, and also has a payload of data.
    * example:
        ```rust
        use lib::dispatch;

        let actionType = 'MYACTION';
        fn input_triggered(payload) {
            dispatch(actionType, payload);
        }
        ```
</br>

2. The dispatch then hits the defined rules for concurrency.
    * The developer can declare how each dispatch is handled, and how each subsequent dispatch (e.g. multiple user inputs in a row) is also handled.
    * Each rule declaration is structured as `rule([actionType], <handler>);`.
    * example:
        ```rust
        use lib::dispatch_rules::{take_every,take_latest};

        fn app_setup () {
            take_every([actionType], myHandlerFunctionA);
            take_latest([actionType], myHandlerFunctionB);
        }
        ```
    * TakeLatest means that only the latest action will be accepted. If a new action is dispatched while the previous action is still being processed, then the previous action will be cancelled.

</br>

3. The developer defined handler must be function with an internal generator. (Note: we refer to these as nested generator functions.)
    * **IMPORTANT:** this requires Rust nightly.
    * example:
        ```rust
        #![feature(generators, generator_trait)]
        use lib::subtasks::{SubTask,join};

        fn gen() -> SubTask!() {
            move || { yield join(1); return 2; }
        }
        ```

</br>

4. Each nested generator function may perform several possible actions:
    * Call another nested generator function.
        * using `fork` or `spawn` declaration
        * A forked generator is attached to the parent, and will be cancelled if the parent is cancelled (e.g. when takeLatest cancels the previous action).
        * A spawned generator is not attached, and will not be cancelled.
    * Return a value back to the parent.
        * using `join` declaration
        * join declarations return the value to the parent.
    * example:
        ```rust
        #![feature(generators, generator_trait)]
        use lib::subtasks::{SubTask,fork,spawn};

        fn gen_f(x) -> SubTask!() {
            move || {}
        }

        fn gen_e() -> SubTask!() {
            move || { yield join(1); }
        }

        fn gen_d() -> SubTask!() {
            move || {
                let res = yield fork(gen_e());
                yield spawn(gen_f(x));
            }
        }

        fn gen_c() -> SubTask!() {
            move || { yield spawn(gen_d()); }
        }
        ```
    * TODO future plan: an `all` declaration, which is a fork on an array of generators.
        * any `join` values returned from all, would be in an array.

</br>

5. Downstream of the handler functions, at some point a state update will be made to the local state model (a.k.a. the store). The developer can declare how downstream state updates are committed.

    * example:
        ```rust
        use lib::{
            dispatch_rules::take_every,
            transactions::{commit,TransactionRule},
        };

        // Declaration of transaction rule
        take_every([actionType], myHandlerFunctionA, TransactionRule::PerRule);

        // Use of a commit
        // TODO -- this commit API is not yet defined
        ```
    * The `take_every([actionType], myHandlerFunctionA, TransactionRule::PerRule);` is your declared rule.
    * Use of `TransactionRule::PerRule` means that all downstream commits from myHandlerFunctionA, will be applied in the same atomic unit. All updates are applied together, or none are applied.


</br>
</br>

# Getting Started

</br>

## How to install

* in bash, project root:
    ```sh
    cargo install <TODO>
    ```

</br>

## Initialize your store:
* TODO: have 3 possible options. Need to design/decide on which.

</br>

## Code up your handlers:
* Use case: declare your side effects and commits.
    * Side Effect rules API. (e.g. `fork`, `spawn`, `join`, `all`)
    * Commit method API. (e.g. `commit`)
* TODO: useful examples with fork, spawn, commit. Maybe link to a dir with examples?

</br>

## Define your Dispatch Rules:
   * Use case: declare dispatch rules during app init.
        * Dispatch rules API: `take_every`, `take_latest`, `take_leading`, `debounce`, `throttle`
        * Transaction rules API: `PerRule`, `PerAction`, `Custom`
   * example:
        ```rust
        use lib::{
            dispatch_rules::{take_every,take_latest},
            transactions::{commit,TransactionRule},
        };

        fn setup_method() {
            take_every(["ACTION_TYPE_1","ACTION_TYPE_2"], generatorA);
            take_latest(["ACTION_TYPE_2"], generatorC, TransactionRule::PerRule);
        }

        fn main() {
            setup_method();
        }
        ```

</br>

## Dispatching an action:
   * Use case: each dispatch action triggers a stream of downstream state management tasks.
        * Dispatch method API: `dispatch`
   * example:
        ```
        TODO: show examples in projects?
        ```


</br>
</br>


# To launch MVP

- [x] Design dispatch flow.
- [x] Define dispatch rules API.
- [ ] Define transaction rules API.
- [ ] Define store model.
- [x] Build: dispatch → task → generator.
- [x] Implement `fork` and `spawn`.
- [ ] Implement `join`.
- [ ] Build scheduler.
- [ ] Build task executor.
- [ ] Implement multi-threaded task execution.
- [ ] Implement task tree cancellation (i.e. gatekeeper + cancel calls).
- [ ] Build store transactions.
- [ ] Implement handling per dispatch rule.
- [ ] Implement handling per transaction rule.
- [ ] Optional: implement `all([])`.
- [ ] Optional: convert global static objects (i.e. gatekeeper & dispatcher) into channels. Then remove all the statics.
- [ ] Ongoing: tests, formatter & linter, documentation.


</br>
</br>


# Contributing
* [Overview of library implementation. Running locally. Etc.](./contributing.md)


