use std::time::{Instant};
use wasm_bindgen::JsValue;
use super::receiver::dispatch_from_receiver;

pub type ActionType<'a> = &'a str;
pub type ActionInitTime = Instant;
pub type ArgVal = Option<JsValue>;

pub fn dispatch (action: ActionType<'static>, val: ArgVal) {
    let time = Instant::now();
    dispatch_from_receiver(time, action, val);
}
