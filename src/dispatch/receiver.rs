use std::{collections::HashMap, pin::Pin, sync::Arc};
use crate::{
    dispatch::actions::ArgVal,
    fork_join_model::SubTaskT,
    transaction::rules::TransactionRule,
    utils::uuid::Uuid,
};
use super::{
    actions::{ActionInitTime, ActionType},
    handler::DispatchHandler,
    rules::DispatchRule,
};

#[derive(Default)]
pub struct Receiver {
    receiver: HashMap<ActionType<'static>, Vec<DispatchHandler>>,
}

// Mutex used on this global dispatch receiver.
// TODO: later on make this a channel in front of the receiver,
// such that all of threads hit the channel (not the mutex)
// and all the static can be removed.
lazy_static! {
    static ref RECEIVER: std::sync::Mutex<Receiver> = std::sync::Mutex::new(Receiver {
        receiver: HashMap::new(),
    });
}

pub fn add_to_receiver<G: SubTaskT> (
    dispatch_rule: DispatchRule,
    action_types: Vec<ActionType<'static>>,
    fun: impl Fn() -> G + Send + Sync + 'static,
    transaction_rule: Option<TransactionRule>
) {
    let fun: Arc<dyn Fn() -> Pin<Box<dyn SubTaskT>> + Send + Sync> = Arc::new(move || Box::pin(fun()));
    for &action_type in action_types.iter() {
        let handler = DispatchHandler::new(
            transaction_rule.unwrap_or(TransactionRule::Unrestricted),
            dispatch_rule,
            fun.clone(),
        );
        RECEIVER.lock().unwrap().receiver.entry(action_type).or_insert(vec![]).push(handler);
    }
}

pub fn dispatch_from_receiver(init_time: ActionInitTime, action: ActionType<'static>, val: ArgVal) {
    if let Some(handlers) = RECEIVER.lock().unwrap().receiver.get_mut(action) {
        for handler in handlers.into_iter() {
            (*handler).dispatch(init_time, action, val.clone());
        }
    }
}

pub fn rm_from_receiver(action_type: ActionType, handler_id: Uuid, action_id: ActionInitTime) {
    if let Some(arr_handlers) = RECEIVER.lock().unwrap().receiver.get_mut(action_type) {
        if let Some (h) = arr_handlers.iter_mut().find(|h| h.get_id() == handler_id) {
            h.rm_task_root(action_id);
        }
    }
}
