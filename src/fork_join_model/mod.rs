/*
    API for users:
        ```
        fn gen_e() -> SubTask!() {
            move || { yield join(1); }
        }

        fn gen_d() -> SubTask!() {
            move || {
                let res = yield fork(gen_e());
                yield spawn(gen_f(x));
            }
        }

        fn gen_c() -> SubTask!() {
            move || { yield spawn(gen_d()); }
        }
        ```
*/

use std::ops::Generator;
use crate::{
    task::generator::{JsValue, GeneratorValue}
};

#[macro_export]
macro_rules! SubTask {
    () => {
        impl std::ops::Generator<Yield = $crate::task::generator::GeneratorValue, Return = ()> + 'static + Send + Sync
    };
}

pub trait SubTaskT = Generator<Yield = GeneratorValue, Return = ()> + 'static + Send + Sync;

pub fn fork (x: SubTask!()) -> GeneratorValue {
    GeneratorValue::Generator(Box::pin(x), true)
}

pub fn spawn (x: SubTask!()) -> GeneratorValue {
    GeneratorValue::Generator(Box::pin(x), false)
}

pub fn join (x: JsValue) -> GeneratorValue {
    GeneratorValue::Value(x)
}
