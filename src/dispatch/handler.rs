use std::{
    collections::HashMap,
    pin::Pin,
    sync::Arc,
};
use crate::{
    fork_join_model::SubTaskT,
    task::{context::Context, task::{Lineage, Task}},
    transaction::rules::TransactionRule,
    utils::uuid::Uuid
};
use super::{actions::{ActionInitTime, ActionType, ArgVal}, rules::DispatchRule};

pub struct DispatchHandler {
    id: Uuid,
    transaction_rule: TransactionRule,
    dispatch_rule: DispatchRule,
    handler_fn: Arc<dyn Fn() -> Pin<Box<dyn SubTaskT>> + Send + Sync>,
    task_roots: HashMap<ActionInitTime, Task>,
}

impl <'a> DispatchHandler {
    pub fn new (
        transaction_rule: TransactionRule,
        dispatch_rule: DispatchRule,
        handler_fn: Arc<dyn Fn() -> Pin<Box<dyn SubTaskT>> + Send + Sync>,
    ) -> Self {
        Self {
            id: Uuid::new(),
            transaction_rule,
            dispatch_rule,
            handler_fn,
            task_roots: HashMap::new(),
        }
    }

    pub fn get_id(&self) -> Uuid {
        self.id
    }

    pub fn dispatch(&mut self, init_time: ActionInitTime, action: ActionType<'static>, val: ArgVal) {
        if self.ok_to_dispatch(init_time) {
            let root_task_id = Uuid::new();
            let context = Context::new(self.id, init_time, root_task_id);
            let gen = (*self.handler_fn)();
            let task = Task::new(Some(root_task_id), gen, val, context, Lineage::Root(action));
            // TODO: schedule task
        }
    }

    fn ok_to_dispatch(&self, init_time: ActionInitTime) -> bool {
        // TODO -- apply dispatch rule. every, latest, debounce, etc
        // this is why the task_roots are keyed to init_time
        true
    }

    fn add_task_root(& mut self, task: Task) {
        self.task_roots.insert(task.get_context().get_dispatch_time(), task);
    }

    pub fn rm_task_root(&mut self, dispatch_time: ActionInitTime) {
        self.task_roots.remove(&dispatch_time);
    }
}
