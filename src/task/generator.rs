/*
    Wrapper for use with Rust generators.
    i.e. when this lib crate is imported and used in Rust-wasm code.
*/
use std::ops::{Generator, GeneratorState};
use std::pin::Pin;

type Attached = bool;
pub type JsValue = u32; // TODO: what are the allowed returned values?
pub type PinnedGenerator = Pin<Box<dyn Generator<Yield = GeneratorValue, Return = ()> + Send + Sync>>;

pub enum GeneratorValue {
    Value(JsValue),
    Generator(PinnedGenerator, Attached),
}

pub enum GeneratorResult {
    SubTask(PinnedGenerator, Attached),
    Join(JsValue),
    Done,
    Error,
}

pub struct WrappedGenerator {
    fun: PinnedGenerator,
    value: Option<JsValue>,
}

impl WrappedGenerator {
    pub fn new(fun: PinnedGenerator) -> Self {
        Self {
            fun,
            value: None,
        }
    }

    pub fn next(&mut self) -> GeneratorResult {
        match self.fun.as_mut().resume(()) {
            GeneratorState::Yielded(x) => {
                match x {
                    GeneratorValue::Value(val) => { return GeneratorResult::Join(val) },
                    GeneratorValue::Generator(gen, att) => {
                        return GeneratorResult::SubTask(gen, att);
                    },
                }
            },
            GeneratorState::Complete(()) => { return GeneratorResult::Done },
            _ => { return GeneratorResult::Error },
        }
    }
}
