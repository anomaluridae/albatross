# Contributing.

Overview of the basic flow of this library:

![uml diagram](./lib_overview.png)


There are a few key synchronization points:
1. the dispatch handler
    * applies the Dispatch rules, per the developer declarations.
2. the scheduler
    * applies the Side Effect rules, per the developer declarations.
3. the gatekeeper
    * applies the Transaction rules, per the developer declarations.

All other work performed in the task trees (e.g. I/O and network calls) occurs in isolation and can be run concurrently and in parallel. Any dispatched action (via user inputs and your own Rust code) can also be multithreaded.


</br>
</br>

# Running the project locally.

**TODO: NOT DONE YET.**
