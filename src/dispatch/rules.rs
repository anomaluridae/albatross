use std::u64;
use crate::{
   fork_join_model::SubTaskT,
   transaction::rules::TransactionRule
};
use super::{
   actions::ActionType,
   receiver::add_to_receiver,
};

#[derive(Clone, Copy, Debug)]
pub enum DispatchRule {
   TakeEvery,
   TakeLatest,
   TakeLeading,
   Debounce(u64), // milliseconds
   Throttle(u64), // milliseconds
}

pub fn take_every<G: SubTaskT>(
   action_types: Vec<ActionType<'static>>,
   fun: impl Fn() -> G + Send + Sync + 'static,
   transaction_rule: Option<TransactionRule>
) {
   add_to_receiver(DispatchRule::TakeEvery, action_types, fun, transaction_rule);
}

pub fn take_latest<G: SubTaskT>(
   action_types: Vec<ActionType<'static>>,
   fun: impl Fn() -> G + Send + Sync + 'static,
   transaction_rule: Option<TransactionRule>
) {
   add_to_receiver(DispatchRule::TakeLatest, action_types, fun, transaction_rule);
}

pub fn take_leading<G: SubTaskT>(
   action_types: Vec<ActionType<'static>>,
   fun: impl Fn() -> G + Send + Sync + 'static,
   transaction_rule: Option<TransactionRule>
) {
   add_to_receiver(DispatchRule::TakeLeading, action_types, fun, transaction_rule);
}

pub fn debounce<G: SubTaskT>(
   time: u64,
   action_types: Vec<ActionType<'static>>,
   fun: impl Fn() -> G + Send + Sync + 'static,
   transaction_rule: Option<TransactionRule>
) {
   add_to_receiver(DispatchRule::Debounce(time), action_types, fun, transaction_rule);
}

pub fn throttle<G: SubTaskT>(
   time: u64,
   action_types: Vec<ActionType<'static>>,
   fun: impl Fn() -> G + Send + Sync + 'static,
   transaction_rule: Option<TransactionRule>
) {
   add_to_receiver(DispatchRule::Throttle(time), action_types, fun, transaction_rule);
}
