use std::sync::{Arc,atomic::{AtomicBool,Ordering}};
use crate::{
    dispatch::{
        actions::{ActionType, ArgVal},
        receiver::rm_from_receiver,
    },
    utils::uuid::Uuid
};
use super::{context::Context, generator::{GeneratorResult, PinnedGenerator, WrappedGenerator}};

const CANCELLED: &str = "cancelled";

pub enum Lineage {
    Root(ActionType<'static>),
    Spawn,
    Fork(Arc<SharedData>),
    Cancel, // drop pointer to parent
}

pub struct SharedData {
    cancelled: AtomicBool,
    done: AtomicBool,
    lineage: Lineage,
    context: Context,
}

impl SharedData {
    /// Bottom-up cancel message.
    /// e.g. API call failed, or gatekeeper triggered cancellation
    fn cancel(&self) {
        self.cancelled.store(true, Ordering::Relaxed);
        self.done.store(true, Ordering::Relaxed);
        self.schedule_for_removal();

        if let Lineage::Fork(p) = &self.lineage {
            p.cancel();
        }
    }

    fn is_done(&self) -> bool {
        self.done.load(Ordering::Relaxed)
    }

    fn has_succeeded(&self) -> bool {
        !self.cancelled.load(Ordering::Relaxed) && self.is_done()
    }

    fn mark_as_done(&self) {
        self.done.store(true, Ordering::Relaxed);
    
        // TODO: how join value back to parent task?
        // re-insert parent task into the scheduler, with the passed return value?
        // if let (Some(p), true) = (self.parent, self.attached) {
        //     p.as_ref().borrow().join();
        // }
    
        self.schedule_for_removal();
    }

    fn is_root_task(&self) -> bool {
        match self.lineage {
            Lineage::Root(_) => true,
            _ => false,
        }
    }

    fn schedule_for_removal(&self) {
        // TODO: remove init_time from Gatekeeper.
        // all subtasks in task tree should halt.

        if let Lineage::Root(action_type) = self.lineage {
            rm_from_receiver(action_type, self.context.handler_id, self.context.get_dispatch_time());
        }
    }
}

pub struct Task {
    id: Uuid,
    fun: WrappedGenerator,
    shared: Arc<SharedData>,
}

impl Task {
    pub fn new(
        uuid: Option<Uuid>, fun: PinnedGenerator, arg: ArgVal, context: Context, lineage: Lineage
    ) -> Self {
        Self {
            id: uuid.unwrap_or(Uuid::new()),
            shared: Arc::new(SharedData {
                cancelled: AtomicBool::new(false),
                done: AtomicBool::new(false),
                lineage,
                context,
            }),
            fun: WrappedGenerator::new(fun),
        }
    }

    pub fn next(&mut self) {
        match self.fun.next() {
            GeneratorResult::SubTask(t, attached) => {
                if attached { self.fork_child_task(t) }
                else { self.spawn_child_task(t) }
            },
            GeneratorResult::Join(v) => {
                // TODO
            },
            GeneratorResult::Done => { self.shared.mark_as_done(); },
            GeneratorResult::Error => {
                // TODO
            },
        }
    }

    fn spawn_child_task(&self, fun: PinnedGenerator) {
        let mut context = self.get_context().clone();
        let new_root_id = Uuid::new();
        context.set_gatekeeper_id(new_root_id);
        let task = Task::new(Some(new_root_id), fun, None, context, Lineage::Spawn);
        // TODO: schedule task
    }

    fn fork_child_task(&self, fun: PinnedGenerator) {
        let context = self.get_context().clone();
        let task = Task::new(None, fun, None, context, Lineage::Fork(self.shared.clone()));
        // TODO: schedule task
    }

    pub fn get_context(&self) -> &Context {
        &self.shared.context
    }

    pub fn is_done(&self) -> bool {
        self.shared.is_done()
    }
}
