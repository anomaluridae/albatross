use crate::{
    dispatch::actions::ActionInitTime,
    utils::uuid::Uuid,
};

#[derive(Clone, Debug)]
pub struct Context {
    pub handler_id: Uuid,
    task_root_id: Uuid,
    init_time: ActionInitTime,
}

impl Context {
    pub fn new(handler_id: Uuid, init_time: ActionInitTime, task_root_id: Uuid) -> Self {
        Self {
            handler_id,
            init_time,
            task_root_id,
        }
    }

    pub fn get_dispatch_time(&self) -> ActionInitTime {
        self.init_time
    }

    pub fn get_gatekeeper_id(&self) -> Uuid {
        self.task_root_id
    }

    pub fn set_gatekeeper_id(&mut self, new: Uuid) {
        self.task_root_id = new;
    }
}
